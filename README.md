# TestRail Reporter for Karma

Publishes Karma runs on TestRail.
It is inspired by: [karma-mocha-reporter](https://github.com/litixsoft/karma-mocha-reporter) and [cypress-testrail-reporter](https://github.com/Vivify-Ideas/cypress-testrail-reporter)

## Install

```shell
$ npm install https://bitbucket.org/overpass/karma-testrail-reporter.git --save-dev
```

## Usage

Add reporter to your `karma.conf.js`:

```json
...
"reporters": ["testrail"],
"testrailReporter": {
	"domain": "yourdomain.testrail.io",
	"username": "username",
	"password": "password",
	"projectId": 1,
	"suiteId": 1,
	"runName": "run name",
	"runDescription": "run description"
}
```

Your Cypress tests should include the ID of your TestRail test case. Make sure your test case IDs are distinct from your test titles:

```Javascript
// Good:
it("C123 C124 Can authenticate a valid user", ...
it("Can authenticate a valid user C321", ...

// Bad:
it("C123Can authenticate a valid user", ...
it("Can authenticate a valid userC123", ...
```

## Reporter Options

**host**: _string_ host of your TestRail instance (e.g. for a hosted instance _https://instance.testrail.com_).

**username**: _string_ email of the user under which the test run will be created.

**password**: _string_ password or the API key for the aforementioned user.

**projectId**: _number_ project with which the tests are associated.

**suiteId**: _number_ suite with which the tests are associated.

**runName**: _string_ (optional) name of the Testrail run.

**runDescription**: _string_ (optional) description of the Testrail run.

## TestRail Settings

To increase security, the TestRail team suggests using an API key instead of a password. You can see how to generate an API key [here](http://docs.gurock.com/testrail-api2/accessing#username_and_api_key).

If you maintain your own TestRail instance on your own server, it is recommended to [enable HTTPS for your TestRail installation](http://docs.gurock.com/testrail-admin/admin-securing#using_https).

For TestRail hosted accounts maintained by [Gurock](http://www.gurock.com/), all accounts will automatically use HTTPS.

You can read the whole TestRail documentation [here](http://docs.gurock.com/).

## License

This project is licensed under the [MIT license](/LICENSE).
