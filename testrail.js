'use strict';

const axios = require('axios');
const chalk = require('chalk');

class TestRail {

	constructor(options) {
		this.base = `https://${options.domain}/index.php?/api/v2`;
		this.options = options;
		this.runId = null;
		this.includeAll = true;
		this.caseIds = [];
	}

	getCases() {
		return axios({
			method: 'get',
			url: `${this.base}/get_cases/${this.options.projectId}&suite_id=${this.options.suiteId}&section_id=${this.options.groupId}&filter=${this.options.filter}`,
			headers: { 'Content-Type': 'application/json' },
			auth: {
				username: this.options.username,
				password: this.options.password
			}
		})
			.then(response => response.data.map(item => item.id))
			.catch(error => console.error(error));
	}

	async createRun(name, description) {
		if (this.options.includeAllInTestRun === false) {
			this.includeAll = false;
			this.caseIds = await this.getCases();
		}
		axios({
			method: 'post',
			url: `${this.base}/add_run/${this.options.projectId}`,
			headers: { 'Content-Type': 'application/json' },
			auth: {
				username: this.options.username,
				password: this.options.password,
			},
			data: JSON.stringify({
				suite_id: this.options.suiteId,
				name,
				description,
				include_all: this.includeAll,
				case_ids: this.caseIds
			}),
		})
			.then(response => {
				this.runId = response.data.id;
			})
			.catch(error => console.error(error));
	}

	deleteRun() {
		axios({
			method: 'post',
			url: `${this.base}/delete_run/${this.runId}`,
			headers: { 'Content-Type': 'application/json' },
			auth: {
				username: this.options.username,
				password: this.options.password,
			},
		}).catch(error => console.error(error));
	}

	publishResults(results) {
		return axios({
			method: 'post',
			url: `${this.base}/add_results_for_cases/${this.runId}`,
			headers: { 'Content-Type': 'application/json' },
			auth: {
				username: this.options.username,
				password: this.options.password,
			},
			data: JSON.stringify({ results }),
		})
			.then(response => {
				console.log('\n', chalk.magenta.underline.bold('(TestRail Reporter)'));
				console.log(
					'\n',
					` - Results are published to ${chalk.magenta(
						`https://${this.options.domain}/index.php?/runs/view/${this.runId}`
					)}`,
					'\n'
				);
				return response;
			})
			.catch(error => console.error(error));
	}

	closeRun() {
		return axios({
			method: 'post',
			url: `${this.base}/close_run/${this.runId}`,
			headers: { 'Content-Type': 'application/json' },
			auth: {
				username: this.options.username,
				password: this.options.password,
			},
		})
			.then(() => {
				console.log(' - Test run closed successfully');
				return true;
			})
			.catch(error => {
				console.error(error);
				return false;
			});
	}
}

module.exports.TestRail = TestRail;