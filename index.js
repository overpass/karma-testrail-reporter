'use strict';

const moment = require('moment');
const chalk = require('chalk');
const { TestRail } = require('./testrail');
const { sleep, titleToCaseIds } = require('./shared');

const Status = {
	Passed: 1,
	Blocked: 2,
	Untested: 3,
	Retest: 4,
	Failed: 5,
}

const KarmaTestrailReporter = function (baseReporterDecorator, config, logger) {

	this.validate = (options, name) => {
		if (options == null) {
			throw new Error('Missing testrailReporter in karma.conf.js');
		}
		if (options[name] == null) {
			throw new Error(`Missing ${name} value. Please update testrailReporter in karma.conf.js`);
		}
	}

	const testrailConfig = config.testrailReporter || {};
	this.validate(testrailConfig, 'domain');
	this.validate(testrailConfig, 'username');
	this.validate(testrailConfig, 'password');
	this.validate(testrailConfig, 'projectId');
	this.validate(testrailConfig, 'suiteId');
	this.testRail = new TestRail(testrailConfig);
	baseReporterDecorator(this);
	this.results = [];

	this.onRunStart = async (browsers) => {
		const executionDateTime = moment().format('MMM Do YYYY, HH:mm (Z)');
		const name = `${testrailConfig.runName || 'Automated test run'} ${executionDateTime}`;
		const description = testrailConfig.runDescription || 'Testing description';
		await this.testRail.createRun(name, description);
	};

	this.onBrowserStart = (browser) => {
		// overwrite the base method
	};

	this.addSpecResult = (result, status, comment) => {
		let caseIds = titleToCaseIds(result.description);
		if (caseIds.length > 0) {
			let results = caseIds.map(caseId => {
				return {
					case_id: caseId,
					status_id: status,
					comment: comment
				};
			});

			this.results.push(...results);
		}
	}

	this.specSuccess = (browser, result) => {
		this.addSpecResult(result, Status.Passed, `${result.description} (${result.time}ms)`);
	}

	this.specFailure = (browser, result) => {
		let logs = result.log.join('\n\n');
		this.addSpecResult(result, Status.Failed, logs);
	};

	this.specSkipped = (browser, result) => {
		this.addSpecResult(result, Status.Untested, `${result.description} (${result.time}ms)`);
	}

	this.onSpecComplete = (browser, result) => {
		if (result.skipped) {
			this.specSkipped(browser, result);
		} else if (result.success) {
			this.specSuccess(browser, result);
		} else {
			this.specFailure(browser, result);
		}
		this.write(result.description);
	}

	this.onRunComplete = async (browsersCollection, results) => {
		if (this.results.length == 0) {
			this.write('\n', chalk.magenta.underline.bold('(TestRail Reporter)'));
			this.write(
				'\n',
				chalk.red('No testcases were matched. Ensure that your tests are declared correctly and matches Cxxx'),
				'\n'
			);
			this.testRail.deleteRun();
			this.closeRunResult = true;
			return;
		}
		
		await this.testRail.publishResults(this.results);
		this.closeRunResult = await this.testRail.closeRun();
		this.write('\n', chalk.magenta.underline.bold('(TestRail Reporter): Test results submitted'), '\n\n');
	};

	this.onExit = async (done) => {
		// wait until the run is closed
		while (this.closeRunResult === undefined) {
			await sleep(500);
		}
		done();
	}
};


KarmaTestrailReporter.$inject = ['baseReporterDecorator', 'config', 'logger'];

// PUBLISH DI MODULE
module.exports = {
	'reporter:testrail': ['type', KarmaTestrailReporter]
};
